//
//  NSDateExtension.swift
//  ISCCore
//
//  Created by Rifat Firdaus on 6/13/16.
//  Copyright © 2016 Suitmedia. All rights reserved.
//

import Foundation

public extension NSDate {
    func localeDateMatchString() -> String {
        let formatter = NSDateFormatter()
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 25200)
        formatter.dateFormat = "EEEE, dd MMMM yyyy"
        var stringDate = formatter.stringFromDate(self)
        
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Monday", withString: "Senin")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Tuesday", withString: "Selasa")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Wednesday", withString: "Rabu")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Thursday", withString: "Kamis")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Friday", withString: "Jumat")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Saturday", withString: "Sabtu")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Sunday", withString: "Minggu")
        
        stringDate = stringDate.stringByReplacingOccurrencesOfString("January", withString: "Januari")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("February", withString: "Februari")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("March", withString: "Maret")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("April", withString: "April")
        // April
        stringDate = stringDate.stringByReplacingOccurrencesOfString("May", withString: "Mei")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("June", withString: "Juni")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("July", withString: "Juli")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("August", withString: "Agustus")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("September", withString: "September")
        // September
        stringDate = stringDate.stringByReplacingOccurrencesOfString("October", withString: "Oktober")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("November", withString: "November")
        // November
        stringDate = stringDate.stringByReplacingOccurrencesOfString("December", withString: "Desember")
        
        return stringDate
    }
    
    func localeDateDetailMatchString() -> String {
        let formatter = NSDateFormatter()
        formatter.timeZone = NSTimeZone(forSecondsFromGMT: 25200)
        formatter.dateFormat = "dd MMMM yyyy"
        var stringDate = formatter.stringFromDate(self)
        
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Monday", withString: "Senin")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Tuesday", withString: "Selasa")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Wednesday", withString: "Rabu")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Thursday", withString: "Kamis")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Friday", withString: "Jumat")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Saturday", withString: "Sabtu")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("Sunday", withString: "Minggu")
        
        stringDate = stringDate.stringByReplacingOccurrencesOfString("January", withString: "Jan")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("February", withString: "Feb")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("March", withString: "Mar")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("April", withString: "Apr")
        // April
        stringDate = stringDate.stringByReplacingOccurrencesOfString("May", withString: "Mei")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("June", withString: "Juni")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("July", withString: "Juli")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("August", withString: "Agt")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("September", withString: "Sep")
        // September
        stringDate = stringDate.stringByReplacingOccurrencesOfString("October", withString: "Okt")
        stringDate = stringDate.stringByReplacingOccurrencesOfString("November", withString: "Nov")
        // November
        stringDate = stringDate.stringByReplacingOccurrencesOfString("December", withString: "Des")
        
        return stringDate
    }
    
    func compareDateWith(otherDate: NSDate?) -> Bool {
        if let otherDate = otherDate {
            let formatter = NSDateFormatter()
            formatter.dateFormat = "yyyyMMdd"
            let stringDate = formatter.stringFromDate(self)
            let stringOtherDate = formatter.stringFromDate(otherDate)
            if stringDate == stringOtherDate {
                return true
            }
            return false
        }
        return false
    }
}
