//
//  RealmExtension.swift
//  Fans Match Center
//
//  Created by Rifat Firdaus on 5/20/16.
//  Copyright © 2016 Suitmedia. All rights reserved.
//

import Foundation

import RealmSwift

public extension Realm {
    
    func objectForPrimaryKey<T: Object>(type: T.Type, key: Int64) -> T? {
        return self.objectForPrimaryKey(type, key: NSNumber(longLong: key))
    }
}
