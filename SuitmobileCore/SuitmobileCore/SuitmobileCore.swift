//
//  SuitmobileCore.swift
//  SuitmobileCore
//
//  Created by Rifat Firdaus on 11/18/16.
//  Copyright © 2016 Suitmedia. All rights reserved.
//

import Foundation
import RealmSwift

public class SuitmobileCore: NSObject {
    public static func setup(home: String, headers: [String:String]? = nil) {
        var config = Realm.Configuration(
            schemaVersion: 1,
            migrationBlock: { (migration, oldSchemaVersion) in
                if (oldSchemaVersion < 1) {
                    print("<<REALM: PROVIDED SCHEMA VERSION IS LESS THAN LAST SET VERSION>>")
                }
            }, deleteRealmIfMigrationNeeded: true, objectTypes: nil)
        Realm.Configuration.defaultConfiguration = config
        let _ = try! Realm()
        if let url = config.fileURL {
            print(url.absoluteString)
        }
        SuitmobileCoreService.instance.home = home
        if let headers = headers {
            SuitmobileCoreService.instance.headers = headers
        }
    }
    
}
