//
//  SuitmobileCoreService.swift
//  SuitmobileCore
//
//  Created by Rifat Firdaus on 11/18/16.
//  Copyright © 2016 Suitmedia. All rights reserved.
//

import Foundation
import Alamofire
import RxAlamofire
import SwiftyJSON
import RealmSwift

import RxSwift
import RxCocoa


open class SuitmobileCoreService: NSObject {
    public static let instance = SuitmobileCoreService()
    
    public var manager = SuitmobileSessionManager()
    public var home = ""
    public let successCode = 200
    
    public var headers = [
        "X-PublicKey" : "beTheOneDangdutZamanNow",
        "X-HashKey" : "d60d9b6777b72ef10e3970ab210c6216"
    ]
}

public class SuitmobileSessionManager: SessionManager {
    override public func request(_ url: URLConvertible, method: HTTPMethod, parameters: Parameters? = nil, encoding: ParameterEncoding = URLEncoding.default, headers: HTTPHeaders? = nil) -> DataRequest {
        do {
            let urlRequest = URLRequest(url: try url.asURL())
            let request = try! encoding.encode(urlRequest, with: parameters)
            print("[\(method.rawValue)] \(request)")
        } catch {
            print("🚫 Error URL Request")
        }
        return super.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers)
    }
    
    public func requestJSON(_ method: Alamofire.HTTPMethod,
                            _ url: URLConvertible,
                            parameters: [String: Any]? = nil,
                            encoding: ParameterEncoding = URLEncoding.default,
                            headers: [String: String]? = nil)
        -> Observable<(HTTPURLResponse, Any)>
    {
        do {
            let urlRequest = URLRequest(url: try url.asURL())
            let request = try! encoding.encode(urlRequest, with: parameters)
            print("➡️ [\(method.rawValue)] \(request)")
        } catch {
            print("🚫 Error URL Request")
        }
        return rx.responseJSON(
            method,
            url,
            parameters: parameters,
            encoding: encoding,
            headers: headers
        )
    }
    
    public func download(urlRequest: URLRequestConvertible, to: @escaping DownloadRequest.DownloadFileDestination) -> Observable<DownloadRequest> {
        do {
            let encoding = URLEncoding.default
            let request = try encoding.encode(urlRequest, with: nil)
            print("➡️ [Download] \(request)")
        } catch {
            print("🚫 Error URL Request Download: \(error.localizedDescription)")
        }
        return rx.download(urlRequest, to: to)
    }
    
}

public struct Pagination<T> {
    public let pageStatus: PageStatus
    public let data: T
    
    public init(pageStatus: PageStatus, data: T) {
        self.pageStatus = pageStatus
        self.data = data
    }
}

public struct PageStatus {
    public var total: Int = 0
    public var currentPage: Int = 1
//    public var nextPage: Int = 1
    public var lastPage: Int = 0
    public var hasNext: Bool = false
    
    public static func with(json: JSON) -> PageStatus {
        var pageStatus = PageStatus()
        pageStatus.total = json["total"].intValue
        pageStatus.currentPage = json["current_page"].intValue
        pageStatus.lastPage = json["last_page"].intValue
        pageStatus.hasNext = false
        if let _ = json["next_page_url"].string {
            pageStatus.hasNext = true
        }
        return pageStatus
    }
}

public enum APIResult<T> {
    case success(T)
    case failure(NSError)
}

