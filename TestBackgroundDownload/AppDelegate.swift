//
//  AppDelegate.swift
//  TestBackgroundDownload
//
//  Created by Alam Akbar Muhammad on 06/08/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import UIKit
import SuitmobileCore
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.all

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // API config
        APIConfig.isProduction = false
        
        // Other config
        //FirebaseApp.configure()
        
        // Setup default style
        setupStatusBarStyle()
        setupSVProgressHUD()
        //DropDown.startListeningToKeyboard()
        
        SuitmobileCore.setup(home: "http://bolnus.suitdev.com")
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        SMRequestDownload.shared.show()
    }
    
    static func getWindow2() -> UIWindow? {
        return UIApplication.shared.windows.first(where: { $0.tag == 100 })
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        print("xxx: \(identifier)")
        //SMRequestDownload.shared.request?.resume()
    }

    // MARK: METHODS
    
    func setupStatusBarStyle() {
        // uncomment if view controller-based status bar style is YES to set default navigation status bar style
        UINavigationBar.appearance().barStyle = .blackOpaque
        
        // uncomment if view controller-based status bar style is NO
        //UIApplication.shared.statusBarStyle = .lightContent
        
        //(UIApplication.shared.value(forKey: "statusBar") as? UIView)?.backgroundColor = UIColor(red: 13, green: 95, blue: 167)
    }
    
    func setupSVProgressHUD() {
        SVProgressHUD.setDefaultStyle(.custom)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.setMaximumDismissTimeInterval(1.25)
        SVProgressHUD.setForegroundColor(UIColor.white)
        SVProgressHUD.setBackgroundColor(UIColor.init(hex: 0x777777, alpha: 0.95))
    }
    
    // STATIC HELPERS
    
    static func relaunchMain(completion: ((Bool) -> (Void))?) {
        //let nav = MainTabBarViewController.instantiateNav()
        //relaunchScreen(controller: controller, completion: completion)
    }
    
    static func relaunchScreen(controller: UIViewController, completion: ((Bool) -> (Void))?) {
        guard let _window = UIApplication.shared.delegate?.window, let window = _window else {
            return
        }
        
        window.backgroundColor = UIColor.white
        window.alpha = 0.0
        window.rootViewController = controller
        window.makeKeyAndVisible()
        
        UIView.transition(with: window, duration: 0.24, options: .transitionCrossDissolve, animations: {
            window.alpha = 1.0
        }, completion: { completed in
            completion?(completed)
        })
    }
    
    static func getPresentedViewController() -> UIViewController? {
        var vc = UIApplication.shared.keyWindow?.rootViewController
        while (vc?.presentedViewController != nil) {
            vc = vc?.presentedViewController
        }
        return vc
    }
    
    static func getRootViewController() -> UIViewController? {
        let vc = UIApplication.shared.keyWindow?.rootViewController
        return vc
    }

}

