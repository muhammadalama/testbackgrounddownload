//
//  DownloadInfoViewController.swift
//  TestBackgroundDownload
//
//  Created by Alam Akbar Muhammad on 07/09/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import UIKit
import MZFormSheetPresentationController

class DownloadInfoViewController: UIViewController {

    var formSheet: MZFormSheetPresentationViewController?
    
    let textLabel = UILabel()
    
    static func instantiateFormSheet() -> MZFormSheetPresentationViewController {
        let controller = DownloadInfoViewController()
        let formSheetController = MZFormSheetPresentationViewController(contentViewController: controller)
        formSheetController.presentationController?.contentViewSize = CGSize(width: UIScreen.main.bounds.width - 16, height: 100)
        formSheetController.presentationController?.shouldCenterVertically = true
        formSheetController.presentationController?.movementActionWhenKeyboardAppears = .moveToTop
        formSheetController.interactivePanGestureDismissalDirection = MZFormSheetPanGestureDismissDirection.all
        formSheetController.presentationController?.shouldDismissOnBackgroundViewTap = true
        formSheetController.contentViewControllerTransitionStyle = .fade
        controller.formSheet = formSheetController
        return formSheetController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textLabel.text = "This is label"
        view.addSubview(textLabel)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        textLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        textLabel.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        textLabel.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        //view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width - 16, height: 100)
    }

}
