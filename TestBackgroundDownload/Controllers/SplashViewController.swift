//
//  SplashViewController.swift
//  TestBackgroundDownload
//
//  Created by Rifat Firdaus on 2/21/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    var buildVersion: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //UIDevice.lockOrientation(.portrait)
        
        if let info = Bundle.main.infoDictionary, let build = info["CFBundleVersion"] as? String {
            buildVersion = Int(build)!
        }
        print("Bundle Version = \(buildVersion)")
        stampDevice()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        PreferenceManager.instance.token = "b4dfefe21fc41f0cabc9de5d822883d4"
        presentMain()
        // used for login
        //        if let _ = PreferenceManager.instance.token {
        //            presentMain()
        //        } else {
        //            presentLogin()
        //        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //UIDevice.lockOrientation(.all)
    }
    
    func presentLogin() {
        
    }
    
    func presentMain() {
        let controller = TemplateTableViewController.instantiate()
        present(controller, animated: true, completion: nil)
    }
    
    func stampDevice() {
        print("📱 DEVICE NAME : \(UIDevice.current.name)")
        print("📱 DEVICE MODEL : \(UIDevice.current.model)")
        print("📱 DEVICE SYSTEM VERSION : \(UIDevice.current.systemName) \(UIDevice.current.systemVersion)")
        print("🖥 SCREEN(WxH) : \(UIScreen.main.bounds.width) x \(UIScreen.main.bounds.height)")
    }

}
