//
//  TemplateTableViewController.swift
//  TestBackgroundDownload
//
//  Created by Rifat Firdaus on 2/21/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import UIKit
import SVPullToRefresh
import RxSwift
import RxCocoa
import SVProgressHUD

class TemplateTableViewController: UIViewController {
    
    var interactor = TemplateTableInteractor()
    let disposeBag = DisposeBag()
    
    // MARK: - Static Method
    
    static func instantiate() -> TemplateTableViewController {
        return UIStoryboard.template.instantiateViewController(withIdentifier: "TemplateTableSID") as! TemplateTableViewController
    }
    
    // MARK: - Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStreams()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //interactor.refreshSubject.onNext(Void())
    }
    
    // MARK: METHODS
    
    func setupStreams() {
        interactor.refreshSubject
            .do(onNext:{
                print("start download")
            })
            .flatMapLatest { self.interactor.testDownload1() }
            .retry()
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    func testDownload1() {
        interactor.testDownload1()
        .subscribe()
        .disposed(by: disposeBag)
    }
    
    func testDownload1WithSubject() {
        interactor.refreshSubject.onNext(Void())
    }
    
    func testDownload2() {
        interactor.testDownload2(onSuccess: {
            SVProgressHUD.showSuccess(withStatus: "Completed")
            self.view.backgroundColor = .lightGray
        }, onError: { error in
            SVProgressHUD.showError(withStatus: error.localizedDescription)
        })
    }
    
    @IBAction func downloadPressed(_ sender: Any) {
        /*
         Choose one of the following test:
         1. testDownload1()
         2. testDownload1WithSubject()
         3. testDownload2()
        */
        
        testDownload1()
    }
    
}
