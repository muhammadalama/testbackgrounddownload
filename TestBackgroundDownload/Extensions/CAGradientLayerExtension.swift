//
//  CAGradientLayerExtension.swift
//  CommHub
//
//  Created by Alam Akbar Muhammad on 29/03/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import Foundation

extension CAGradientLayer {
    
    convenience init(frame: CGRect, colors: [UIColor]) {
        self.init()
        self.frame = frame
        self.colors = []
        for color in colors {
            self.colors?.append(color.cgColor)
        }
        startPoint = CGPoint(x: 0, y: 0)
        endPoint = CGPoint(x: 1, y: 0)
    }
    
    func createGradientImage() -> UIImage? {
        var image: UIImage? = nil
        UIGraphicsBeginImageContext(bounds.size)
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
        }
        UIGraphicsEndImageContext()
        return image
    }
    
    func createGradientLayer(firstColor: UIColor, secondColor: UIColor, bounds: CGRect) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [firstColor.cgColor, secondColor.cgColor]
        return layer
    }
    
}
