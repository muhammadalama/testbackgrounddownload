//
//  DateExtension.swift
//  Bali United
//
//  Created by Muhammad Alam Akbar on 3/9/17.
//  Copyright © 2017 Suitmedia. All rights reserved.
//

import Foundation
import SwiftDate

extension Date {
    
    func formatIntervalAgo() -> String {
        let date1: Date = Date() // Same you did before with timeNow variable
        let date2: Date = self
        
        let calender:Calendar = Calendar.current
        let components: DateComponents = calender.dateComponents([.year, .month, .day, .hour, .minute, .second], from: date2, to: date1)
        //print(components)
        var returnString:String = ""
        //print(components.second)
        
        if components.year! >= 1 {
            if components.year! == 1 {
                returnString = String(describing: components.year!)+" year ago"
            } else {
                returnString = String(describing: components.year!)+" years ago"
            }
        } else if components.month! >= 1 {
            if components.month! == 1 {
                returnString = String(describing: components.month!)+" month ago"
            } else {
                returnString = String(describing: components.month!)+" months ago"
            }
        } else if components.day! >= 1 {
            if components.day! == 1 {
                returnString = String(describing: components.day!) + " day ago"
            } else {
                returnString = String(describing: components.day!) + " days ago"
            }
        } else if components.hour! >= 1 {
            if components.hour! == 1 {
                returnString = String(describing: components.hour!) + " hour ago"
            } else {
                returnString = String(describing: components.hour!) + " hours ago"
            }
        } else if components.minute! >= 1 {
            if components.minute! == 1 {
                returnString = String(describing: components.minute!) + " minute ago"
            } else {
                returnString = String(describing: components.minute!) + " minutes ago"
            }
        } else if components.second! < 60 {
            returnString = "Just now"
        }
        
        return returnString
    }
    
    func convertToString(withFormat format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        return formatter.string(from: self)
    }
    
}
