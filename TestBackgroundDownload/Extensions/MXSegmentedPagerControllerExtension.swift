//
//  MXSegmentedPagerControllerExtension.swift
//  Bali United
//
//  Created by Rifat Firdaus on 1/13/17.
//  Copyright © 2017 Suitmedia. All rights reserved.
//

import UIKit

extension MXSegmentedPagerController {
    
    func defaultPagerStyle() {
        // Segmented Control customization
        let barbuttonFont = UIFont.systemFont(ofSize: 13)
        let barbuttonFontSelected = UIFont.boldSystemFont(ofSize: 13)
        //let barbuttonFont = UIFont.ProximaNova(size: 15, style: ProximaNovaStyle.regular)
        //let barbuttonFontSelected = UIFont.ProximaNova(size: 15, style: ProximaNovaStyle.bold)
        segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        segmentedPager.segmentedControl.backgroundColor = .white
        segmentedPager.segmentedControl.borderType = .bottom
        segmentedPager.segmentedControl.hmBorderWidth = 1.0
        segmentedPager.segmentedControl.hmBorderColor = UIColor(red: 225, green: 225, blue: 225)
        segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 178, green: 178, blue: 178), NSAttributedStringKey.font: barbuttonFont];
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor(red: 13, green: 95, blue: 167), NSAttributedStringKey.font: barbuttonFontSelected]
        segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyle.fullWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorColor = UIColor(red: 13, green: 95, blue: 167)
        segmentedPager.segmentedControl.selectionIndicatorHeight = 2.0
        //        segmentedPager.segmentedControl.segmentWidthStyle = .dynamic
    }
    
    func defaultPagerStyleWithParallax() {
        
        // Parallax Header
//        segmentedPager.parallaxHeader.view = HeaderMX.instanceFromNib()
//        segmentedPager.parallaxHeader.mode = MXParallaxHeaderMode.fill
//        segmentedPager.parallaxHeader.height = UIScreen.main.bounds.width / 1.5
//        segmentedPager.parallaxHeader.minimumHeight = 20
        
        
        // Segmented Control customization
        segmentedPager.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocation.down
        // will be used
        //        segmentedPager.segmentedControl.backgroundColor = UIColor(hex:0xD91A31)
        segmentedPager.segmentedControl.backgroundColor = UIColor.clear
        segmentedPager.segmentedControl.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor(hex:0x646464), NSAttributedStringKey.font: UIFont.PFBeauFont(size: 13)]
        segmentedPager.segmentedControl.selectedTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor(hex:0xfcb61e), NSAttributedStringKey.font: UIFont.PFBeauFont(size: 13)]
        segmentedPager.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyle.textWidthStripe
        segmentedPager.segmentedControl.selectionIndicatorColor = UIColor(hex:0xfcb61e)
        segmentedPager.segmentedControl.selectionIndicatorHeight = 1.5
        segmentedPager.segmentedControl.selectionIndicatorEdgeInsets = UIEdgeInsetsMake(0, 0, -11, 0)
        segmentedPager.segmentedControl.type = .fixed
        segmentedPager.segmentedControl.widthFixedType = 12
    }
    
}
