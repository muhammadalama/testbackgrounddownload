//
//  UINavigationBarExtension.swift
//  CommHub
//
//  Created by Alam Akbar Muhammad on 29/03/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import Foundation

extension UINavigationBar {
    
    func setGradientBackground(colors: [UIColor]) {
        var updatedFrame = bounds
        updatedFrame.size.height += self.frame.origin.y
        let gradientLayer = CAGradientLayer(frame: updatedFrame, colors: colors)
        
        setBackgroundImage(gradientLayer.createGradientImage(), for: UIBarMetrics.default)
    }
    
}
