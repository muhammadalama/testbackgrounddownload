//
//  UIStoryboard+Additions.swift
//  Soundrenaline
//
//  Created by Wito Chandra on 13/07/16.
//  Copyright © 2016 Suitmedia. All rights reserved.
//

import UIKit

enum UIStoryboardEnum: String {
    case main, order, history, profile
}

extension UIStoryboard {

    static var template: UIStoryboard {
        return UIStoryboard(name: "Template", bundle: nil)
    }
    
    static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
    
}
