//
//  UITextFieldExtension.swift
//  CommHub
//
//  Created by Alam Akbar Muhammad on 23/03/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import Foundation

extension UITextField {
    
    func placeholder(color: UIColor) {
        let colorAlpha = color.withAlphaComponent(0.7)
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [
            NSAttributedStringKey.foregroundColor: colorAlpha,
            //NSAttributedStringKey.font: UIFont.ProximaNova(size: self.font?.pointSize, style: ProximaNovaStyle.regularItalic)])
            NSAttributedStringKey.font: UIFont.italicSystemFont(ofSize: self.font!.pointSize)])
    }
    
}
