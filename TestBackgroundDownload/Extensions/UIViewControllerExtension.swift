//
//  UIViewController+Extensions.swift
//  Bali United
//
//  Created by Rifat Firdaus on 1/11/17.
//  Copyright © 2017 Suitmedia. All rights reserved.
//

import UIKit
import SVProgressHUD
import Realm
import RealmSwift

enum NavItemType {
    case close
    case back
    case search
    case menu
    case setting
    case bookmark
    case checkmark
    
    func icon() -> UIImage? {
        switch self {
        case .close:
            return #imageLiteral(resourceName: "ic_close_white")
        case .back:
            return #imageLiteral(resourceName: "ic_arrow_back")
        case .search:
            return #imageLiteral(resourceName: "ic_search")
        case .menu:
            return #imageLiteral(resourceName: "ic_more")
        case .setting:
            return #imageLiteral(resourceName: "ic_setting")
        case .bookmark:
            return #imageLiteral(resourceName: "ic_bookmark_big_inactive")
        case .checkmark:
            return #imageLiteral(resourceName: "ic_done")
        }
    }
    
    func selector() -> Selector? {
        switch self {
        case .close:
            return #selector(UIViewController.closePressed(sender:))
        case .back:
            return #selector(UIViewController.backPressed(sender:))
        case .search:
            return #selector(UIViewController.searchPressed(sender:))
        default:
            return nil
        }
    }
}

extension UIViewController {
    
    class func instantiate(_ storyboardEnum: UIStoryboardEnum, _ identifier: String? = nil) -> Self {
        return instantiateHelper(storyboardEnum, identifier)
    }
    
    private class func instantiateHelper<T: UIViewController>(_ storyboardEnum: UIStoryboardEnum, _ identifier: String? = nil) -> T {
        var _identifier = String(describing: T.self).replacingOccurrences(of: "ViewController", with: "")
        _identifier = _identifier.replacingOccurrences(of: "TabBarController", with: "")
        _identifier = identifier ?? _identifier
        //print("Will load: \(_identifier) from \(String(describing: T.self))")
        let storyboard = UIStoryboard(name: storyboardEnum.rawValue.capitalized, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: _identifier) as! T
        return controller
    }
    
    class func instantiateNav(_ storyboardEnum: UIStoryboardEnum, _ identifier: String? = nil) -> UINavigationController {
        var _identifier = String(describing: self).replacingOccurrences(of: "ViewController", with: "")
        _identifier = _identifier.replacingOccurrences(of: "TabBarController", with: "")
        _identifier = identifier ?? _identifier
        return instantiateHelper(storyboardEnum, _identifier + "Nav") as! UINavigationController
    }
    
    func setupNavSearch(delegate: UISearchBarDelegate?) {
        let searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 275, height: 44))
        searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        searchBar.searchBarStyle = .minimal
        searchBar.tintColor = .white
        searchBar.placeholder = "Search"
        searchBar.delegate = delegate
        
        let searchTextField = searchBar.value(forKey: "searchField") as? UITextField
        searchTextField?.textColor = .white
        searchTextField?.leftView = nil
        
        let placeholderLabel = searchTextField?.value(forKey: "placeholderLabel") as? UILabel
        placeholderLabel?.textColor = .white
        
        let clearButton = searchTextField?.value(forKey: "clearButton") as! UIButton
        let img = clearButton.image(for: .normal)?.withRenderingMode(.alwaysTemplate)
        clearButton.setImage(img, for: .normal)
        clearButton.setImage(img, for: .highlighted)
        clearButton.tintColor = .white
        
        navigationItem.titleView = searchBar
        navigationController?.view.layoutIfNeeded()
    }
    
    func setupNavLeftItems(title: String? = "", itemTypes: [NavItemType]) {
        var items = [UIBarButtonItem]()
        for item in itemTypes {
            let selector = item.selector()
            var barButtonItem: UIBarButtonItem
            barButtonItem = UIBarButtonItem(image: item.icon(), style: .plain, target: self, action: selector)
            barButtonItem.tintColor = UIColor.white
            items.append(barButtonItem)
        }
        if let title = title, title != "" {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 150, height: 60))
            label.textColor = UIColor.white
            label.textAlignment = .left
            label.text = title
            //label.font = UIFont.ProximaNova(size: 15)
            label.font = UIFont.systemFont(ofSize: 15)
            let barButtonItemMenu = UIBarButtonItem(customView: label)
            items.append(barButtonItemMenu)
        }
        navigationItem.leftBarButtonItems = items
    }
    
    func setupNavRightItems(target: Any? = nil, itemTypes: [NavItemType], selectors: [Selector?]? = nil) {
        var items = [UIBarButtonItem]()
        for (i, item) in itemTypes.reversed().enumerated() {
            let selector = selectors?.reversed()[i]
            let _target = target == nil ? self : target
            let barButtonItemClose = UIBarButtonItem(image: item.icon(), style: .plain, target: _target, action: selector)
            barButtonItemClose.tintColor = UIColor.white
            items.append(barButtonItemClose)
        }
        navigationItem.rightBarButtonItems = nil
        navigationItem.rightBarButtonItems = items
    }
    
    func setupTitle(_ title: String?) {
        //        navigationItem.leftBarButtonItems = nil
        //        var items = [UIBarButtonItem]()
        //        if let title = title {
        //            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 60))
        //            label.textColor = UIColor.white
        //            label.textAlignment = .left
        //            label.text = title
        //            let barButtonItemMenu = UIBarButtonItem(customView: label)
        //            items.append(barButtonItemMenu)
        //        }
        //        navigationItem.leftBarButtonItems = items
        
        //        let label = UILabel()
        //        label.font = UIFont.ProximaNova(size: 16, style: ProximaNovaStyle.semiBold)
        //        label.textColor = UIColor.white
        //        label.textAlignment = .center
        //        label.text = title
        //        label.sizeToFit()
        //        navigationItem.titleView = label
        navigationItem.title = title
    }
    
    /* NAVIGATION BAR STYLE */
    
    func setupNavbarTransparent() {
        self.navigationController?.navigationBar.backgroundColor = nil
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    func setupMainNavbar(withTitle title: String? = nil) {
        if let title = title {
            // no change if nil
            navigationItem.title = title
        }
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor(red: 13, green: 95, blue: 167)
        navigationController?.navigationBar.setGradientBackground(colors: [UIColor(red: 12, green: 130, blue: 187), UIColor(red: 13, green: 95, blue: 167)])
    }
    
    func setupMainNavbarSecondary(withTitle title: String? = nil) {
        if let title = title {
            // no change if nil
            navigationItem.title = title
        }
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.white
        navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.barStyle = .default
        
        setupNavbarShadow()
    }
    
    func setupNavbarShadow() {
        navigationController?.navigationBar.shadowColor = UIColor.lightGray
        navigationController?.navigationBar.shadowOpacity = 0.8
        navigationController?.navigationBar.shadowOffset = CGSize(width: 0, height: 2.0)
        navigationController?.navigationBar.shadowRadius = 2
    }
    
    func setupNavbarScrollShadow() -> UIView {
        setupNavbarTransparent()
        let gradient = UIImageView(image: UIImage(named: "gradient"))
        gradient.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 64)
        self.view.addSubview(gradient)
        let navBarView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 64))
        navBarView.backgroundColor = UIColor(hex: 0xd91a31)
        return navBarView
    }

    func setupTextRightItems(title: String? = "") {
        var items = [UIBarButtonItem]()
        if let title = title {
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 80, height: 60))
            label.textColor = UIColor.white
            label.textAlignment = .right
            label.text = title
            label.font = UIFont.systemFont(ofSize: 12)
            let barButtonItemMenu = UIBarButtonItem(customView: label)
            items.append(barButtonItemMenu)
        }
        navigationItem.rightBarButtonItems = items
    }
    
    func popToViewController(controllerClass: AnyClass) {
        if let nav = self.navigationController {
            for controller in nav.viewControllers as Array {
                if controller.isKind(of: controllerClass) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
    
    func setGradientBackground(firstColor: UIColor, secondColor: UIColor) {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.colors = [firstColor.cgColor, secondColor.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 1.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        self.view.layer.insertSublayer(gradient, at: 0)
    }
    
    func isModal() -> Bool {
        if self.navigationController?.viewControllers.count == 1 {
            return true
        }
        return false
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    // MARK: SELECTOR
    
    @objc func closePressed(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func backPressed(sender: UIBarButtonItem) {
        // hide search on click back
        //        if let searchBar = navigationItem.titleView, let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, searchTextField.isEditing {
        //            searchBar.resignFirstResponder()
        //            return
        //        }

        if isModal() {
            self.dismiss(animated: true, completion: nil)
        } else {
            let _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func searchPressed(sender: UIBarButtonItem) {
        print("Search!")
    }
    
    @objc func dismissKeyboard(_ sender: UITapGestureRecognizer?) {
        view.endEditing(true)
        
        if let searchBar = navigationItem.titleView, let searchTextField = searchBar.value(forKey: "searchField") as? UITextField, searchTextField.isEditing {
            searchBar.resignFirstResponder()
        }
    }
    
}
