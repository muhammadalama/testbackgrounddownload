//
//  NSURLExtension.swift
//  Bali United
//
//  Created by Muhammad Alam Akbar on 3/23/17.
//  Copyright © 2017 Suitmedia. All rights reserved.
//

import Foundation

extension URL {
    
    func getKeyVals() -> Dictionary<String, String>? {
        var results = [String:String]()
        if let query = self.query {
            var keyValues = query.components(separatedBy: "&")
            if keyValues.count > 0 {
                for pair in keyValues {
                    let kv = pair.components(separatedBy: "=")
                    if kv.count > 1 {
                        results.updateValue(kv[1], forKey: kv[0])
                    }
                }
            }
        }
        return results
    }
    
    func openSafari() {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(self, options: [:])
        } else {
            UIApplication.shared.openURL(self)
        }
    }
    
}
