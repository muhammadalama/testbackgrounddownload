//
//  DesignableView.swift
//  CommHub
//
//  Created by Alam Akbar Muhammad on 24/03/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import UIKit

@IBDesignable
class DesignableView: UIView {
    
}

@IBDesignable
class DesignableImageView: UIImageView {
    
}

@IBDesignable
class DesignableButton: UIButton {
    
}

@IBDesignable
class DesignableTextField: UITextField {
    
}

@IBDesignable
class GradientView: UIView {
    
    @IBInspectable var firstColor:UIColor = UIColor.clear
    @IBInspectable var secondColor:UIColor = UIColor.clear
    @IBInspectable var startPoint:CGPoint = CGPoint(x: 0.0, y: 1.0)
    @IBInspectable var endPoint:CGPoint = CGPoint(x: 1.0, y:0.0)
    
    var gradientLayer:CAGradientLayer!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if let gradientLayer = self.gradientLayer {
            gradientLayer.removeFromSuperlayer()
            self.gradientLayer = nil
        }
        gradientLayer = CAGradientLayer()
        self.gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        self.gradientLayer.startPoint = self.startPoint
        self.gradientLayer.endPoint = self.endPoint
        self.gradientLayer.frame = self.frame
        self.layer.addSublayer(self.gradientLayer)
    }
    
}
