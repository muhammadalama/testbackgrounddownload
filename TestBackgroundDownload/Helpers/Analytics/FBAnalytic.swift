//
//  FBAnalytic.swift
//  Klink Ecommerce
//
//  Created by Alam Akbar Muhammad on 08/03/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

/*
import Foundation
import FacebookCore

struct FBAnalytic {
    
    static let EVENT_PURCHASED_TYPE_REGISTER = "register"
    static let EVENT_PURCHASED_TYPE_SALES = "sales"
    
    static let CURRENCY_IDR = "IDR"
    
    static func sendEventPurchasedTest() {
        AppEventsLogger.log(.purchased(amount: 800, currency: "IDR", extraParameters: [
            AppEventParameterName.currency: CURRENCY_IDR,
            AppEventParameterName.contentType: "test"
            ]))
    }
    
    static func sendEventPurchasedRegister(amount: Double) {
        AppEventsLogger.log(.purchased(amount: amount, currency: "IDR", extraParameters: [
            AppEventParameterName.currency: CURRENCY_IDR,
            AppEventParameterName.contentType: FBAnalytic.EVENT_PURCHASED_TYPE_REGISTER
            ]))
    }
    
    static func sendEventPurchasedSales(amount: Double) {
        AppEventsLogger.log(.purchased(amount: amount, currency: "IDR", extraParameters: [
            AppEventParameterName.currency: CURRENCY_IDR,
            AppEventParameterName.contentType: FBAnalytic.EVENT_PURCHASED_TYPE_SALES
            ]))
    }
    
}

 */
