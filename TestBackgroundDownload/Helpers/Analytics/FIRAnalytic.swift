//
//  FIRAnalytic.swift
//  Klink Ecommerce
//
//  Created by Alam Akbar M on 10/17/17.
//  Copyright © 2017 Suitmedia. All rights reserved.
//

/*
import Foundation
import Firebase

class FIRAnalytic {
    
    //static let TRACKING_ID = "UA-108266056-2"
    
    /* SCREENS */
    // user management
    static let SCREEN_LOGIN = "Login"
    static let SCREEN_FORGOT_PASSWORD = "Forgot Password"
    
    // main screen
    static let SCREEN_HOME = "Home"
    static let SCREEN_NOTIFICATIONS = "Notifications"
    static let SCREEN_WISHLIST = "Wishlist"
    static let SCREEN_CART = "Cart"
    static let SCREEN_PAYMENT_SAL_STEP_1 = "Sales Payment Step 1"
    static let SCREEN_PAYMENT_SAL_STEP_2 = "Sales Payment Step 2"
    static let SCREEN_PAYMENT_SAL_STEP_3 = "Sales Payment Step 3"
    static let SCREEN_PAYMENT_SAL_STEP_4 = "Sales Payment Step 4"
    static let SCREEN_PAYMENT_REG_STEP_2 = "Register Payment Step 2"
    static let SCREEN_PAYMENT_REG_STEP_3 = "Register Payment Step 3"
    static let SCREEN_PAYMENT_REG_STEP_4 = "Register Payment Step 4"
    static let SCREEN_PRODUCT_CATEGORY = "Product category"
    static let SCREEN_VARIAN_DETAIL = "Varian detail"
    static let SCREEN_SEARCH_PRODUCT = "Search product"
    static let SCREEN_TRANSACTION = "Transaction"
    static let SCREEN_TRANSACTION_DETAIL = "Transaction detail"
    static let SCREEN_MY_KLINK = "My K Link"
    
    // more
    static let SCREEN_REGISTER_STEP_1 = "Register Step 1"
    static let SCREEN_REGISTER_STEP_2 = "Register Step 2"
    static let SCREEN_REGISTER_STEP_3 = "Register Step 3"
    static let SCREEN_CATALOGUE = "Catalogue"
    static let SCREEN_PROMOTION = "Promotion"
    static let SCREEN_CHALLENGE_PROMOTION = "Challenge promotion"
    static let SCREEN_INFO_KLINK = "Info K-Link/jadwal"
    static let SCREEN_FILTER = "Filter"
    static let SCREEN_MATERIAL_PRESENTASI = "Material presentasi"
    static let SCREEN_BRIEFCASE = "Briefcase"
    static let SCREEN_SETTINGS = "Settings"
    
    /* EVENTS */
    static let EVENT_ADD_TO_CART = "ADD_TO_CART"
    static let EVENT_PAYMENT_SAL_STEP_1 = "SALES_PAYMENT_STEP_1"
    static let EVENT_PAYMENT_SAL_STEP_2 = "SALES_PAYMENT_STEP_2"
    static let EVENT_PAYMENT_SAL_STEP_3 = "SALES_PAYMENT_STEP_3"
    static let EVENT_PAYMENT_SAL_STEP_4 = "SALES_PAYMENT_STEP_4"
    static let EVENT_PAYMENT_REG_STEP_1 = "REGISTER_PAYMENT_STEP_1"
    static let EVENT_PAYMENT_REG_STEP_2 = "REGISTER_PAYMENT_STEP_2"
    static let EVENT_PAYMENT_REG_STEP_3 = "REGISTER_PAYMENT_STEP_3"
    static let EVENT_PAYMENT_REG_STEP_4 = "REGISTER_PAYMENT_STEP_4"
    static let EVENT_VIEW_VARIANT_DETAIL = "VIEW_VARIANT_DETAIL"
    static let EVENT_CLICK_VARIANT = "CLICK_VARIANT"
    static let EVENT_CLICK_CATEGORY = "CLICK_CATEGORY"
    static let EVENT_CHALLENGE = "CHALLENGE"
    static let EVENT_DOWNLOAD_FILE = "DOWNLOAD_FILE"
    static let EVENT_UPDATE_PROFILE = "UPDATE_PROFILE"
    static let EVENT_PASSWORD_CHANGED = "PASSWORD_CHANGED"
    static let EVENT_LOGOUT = "LOGOUT"
    static let EVENT_OTP_VERIFICATION = "OTP_VERIFICATION"
    static let EVENT_SHARE = "SHARE"
    static let EVENT_DOWNLOAD_STATEMENT = "DOWNLOAD_STATEMENT"
    
    static let SHIPPING_TYPE_STOCKIEST = "stockiest"
    
    static func sendScreen(_ screenName: String) {
        Analytics.setScreenName(screenName, screenClass: nil)
    }
    
    static func sendEvent(eventName: String, params: [String : NSObject]) {
        Analytics.logEvent(eventName, parameters: params)
    }
    
    // user : ID member - member name
    // product : product name
    // quantity : quantity
    static func sendEventAddToCart(memberId: String, memberName: String, productName: String, qty: Int) {
        sendEvent(eventName: EVENT_ADD_TO_CART, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            "product": productName as NSObject,
            "quantity": qty as NSObject
            ])
    }
    
    // user : ID member - member name
    // shipping type : stockiest/nama_ekspedisi
    static func sendEventPaymentRegStep1(memberId: String, memberName: String, shippingType: String) {
        sendEvent(eventName: EVENT_PAYMENT_REG_STEP_1, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            "shipping_type": shippingType as NSObject
            ])
    }
    
    // user : ID member - member name
    // method : payment method name
    static func sendEventPaymentRegStep2(memberId: String, memberName: String, payMethodName: String) {
        sendEvent(eventName: EVENT_PAYMENT_REG_STEP_2, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            "method": payMethodName as NSObject
            ])
    }
    
    // user : ID member - member name
    // products : product name (qty), product name (qty), ...
    // total price : price
    static func sendEventPaymentRegStep3(memberId: String, memberName: String, productsString: String, totalPriceRp: String) {
        sendEvent(eventName: EVENT_PAYMENT_REG_STEP_3, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            "products": productsString as NSObject,
            "total_price": totalPriceRp as NSObject
            ])
    }
    
    // user : ID member - member name
    static func sendEventPaymentRegStep4(memberId: String, memberName: String) {
        sendEvent(eventName: EVENT_PAYMENT_REG_STEP_4, params: [
            "user": "\(memberId) - \(memberName)" as NSObject
            ])
    }

    // user : ID member - member name
    // shipping type : stockiest/nama_ekspedisi
    static func sendEventPaymentSalesStep1(memberId: String, memberName: String, shippingType: String) {
        sendEvent(eventName: EVENT_PAYMENT_SAL_STEP_1, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            "shipping_type": shippingType as NSObject
            ])
    }
    
    // user : ID member - member name
    // method : payment method name
    static func sendEventPaymentSalesStep2(memberId: String, memberName: String, payMethodName: String) {
        sendEvent(eventName: EVENT_PAYMENT_SAL_STEP_2, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            "method": payMethodName as NSObject
            ])
    }
    
    // user : ID member - member name
    // products : product name (qty), product name (qty), ...
    // total price : price
    static func sendEventPaymentSalesStep3(memberId: String, memberName: String, productsString: String, totalPriceRp: String) {
        sendEvent(eventName: EVENT_PAYMENT_SAL_STEP_3, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            "products": productsString as NSObject,
            "total_price": totalPriceRp as NSObject
            ])
    }
    
    // user : ID member - member name
    static func sendEventPaymentSalesStep4(memberId: String, memberName: String) {
        sendEvent(eventName: EVENT_PAYMENT_SAL_STEP_4, params: [
            "user": "\(memberId) - \(memberName)" as NSObject
            ])
    }
    
    // variant : varian name
    static func sendEventViewVariantDetail(variantName: String) {
        sendEvent(eventName: EVENT_VIEW_VARIANT_DETAIL, params: [
            "variant": variantName as NSObject
            ])
    }
    
    // variant : variant name
    static func sendEventClickVariant(variantName: String) {
        sendEvent(eventName: EVENT_CLICK_VARIANT, params: [
            "variant": variantName as NSObject
            ])
    }
    
    // category : category name
    static func sendEventClickCategory(categoryName: String) {
        sendEvent(eventName: EVENT_CLICK_CATEGORY, params: [
            "category": categoryName as NSObject
            ])
    }
    
    // challenge : challenge name
    static func sendEventChallenge(challengeName: String) {
        sendEvent(eventName: EVENT_CHALLENGE, params: [
            "challenge": challengeName as NSObject
            ])
    }
    
    // user : id member - member name
    // file : file name
    static func sendEventDownloadFile(memberId: String, memberName: String, fileName: String) {
        sendEvent(eventName: EVENT_DOWNLOAD_FILE, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            "file": fileName as NSObject
            ])
    }
    
    // user : id member - member name
    static func sendEventUpdateProfile(memberId: String, memberName: String) {
        sendEvent(eventName: EVENT_UPDATE_PROFILE, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            ])
    }
    
    // user : id member - member name
    static func sendEventPasswordChanged(memberId: String, memberName: String) {
        sendEvent(eventName: EVENT_PASSWORD_CHANGED, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            ])
    }
    
    // user : id member - member name
    static func sendEventLogout(memberId: String, memberName: String) {
        sendEvent(eventName: EVENT_LOGOUT, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            ])
    }
    
    // user : id member - member name
    static func sendEventOtpVerification(memberId: String, memberName: String) {
        sendEvent(eventName: EVENT_OTP_VERIFICATION, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            ])
    }
    
    // user : id member - member name
    static func sendEventShare(memberId: String, memberName: String) {
        sendEvent(eventName: EVENT_SHARE, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            ])
    }
    
    // user : id member - member name
    static func sendEventDownloadStatement(memberId: String, memberName: String, date: String) {
        sendEvent(eventName: EVENT_DOWNLOAD_STATEMENT, params: [
            "user": "\(memberId) - \(memberName)" as NSObject,
            "date" : "\(date)" as NSObject
            ])
    }
    
}

 */
