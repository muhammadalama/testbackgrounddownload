//
//  GATracking.swift
//  Bali United
//
//  Created by Bungkhus on 4/11/17.
//  Copyright © 2017 Suitmedia. All rights reserved.
//

/*
class GATracking: NSObject {
    
    static let TRACKING_ID = "UA-97206216-2"
    
    // Screen Tracking
    //
    static let SCREEN_LOGIN = "Login"
    static let SCREEN_LOGIN_EMAIL = "Login Email"
    static let SCREEN_REGISTER = "Register"
    static let SCREEN_FORGOT_PASSWORD = "Forgot Password"
    static let SCREEN_HOME = "Home"
    static let SCREEN_NEWS = "News"
    static let SCREEN_QUIZ_LIST = "Quiz LIst"
    static let SCREEN_QUIZ_DETAIL = "Quiz Detail"
    static let SCREEN_MATCH_RESULT = "Match Result"
    static let SCREEN_MATCH_TABLE = "Match Table"
    static let SCREEN_MATCH_FIXTURE = "Match Fixtures"
    static let SCREEN_VIDEO = "Video"
    static let SCREEN_DETAIL_PROFILE = "Detail profile"
    static let SCREEN_REWARD_POINTS = "Reward Points"
    static let SCREEN_MY_STICKERS = "My Stickers"
    static let SCREEN_NOTIFICATION = "Notification"
    static let SCREEN_EDIT_PROGILE = "Edit profile"
    static let SCREEN_HOME_SEARCH = "Home Search"
    static let SCREEN_NEWS_SEARCH = "News Search"
    static let SCREEN_NEWS_DETAIL = "News Detail"
    static let SCREEN_MATCH_RESULT_DETAIL_LINEUP = "Match Result Line Up"
    static let SCREEN_MATCH_RESULT_DETAIL_REVIEW = "Match Result Review"
    static let SCREEN_MATCH_RESULT_DETAIL_STATISTIC = "Match Result Statistic"
    static let SCREEN_MATCH_RESULT_DETAIL_VIDEO = "Match Result Video"
    static let SCREEN_VIDEO_SEARCH = "Video Search"
    static let SCREEN_VIDEO_DETAIL = "Video detail"
    static let SCREEN_REWARD_POINT_ALL_REWARDS_LIST = "Reward Points All Rewards List"
    static let SCREEN_REWARD_POINT_ALL_REWARDS_DETAIL = "Reward Points All Rewards Detail"
    static let SCREEN_REWARD_POINT_MY_REWARDS_LIST = "Reward Points My Rewards List"
    static let SCREEN_REWARD_POINT_MY_REWARDS_DETAIL = "Reward Points My Rewards Detail"
    static let SCREEN_EARNED_POINTS = "History Points - Earned"
    static let SCREEN_SPENT_POINTS = "History Points - Spent"
    static let SCREEN_STICKERS_DETAIL = "Stickers detail"
    static let SCREEN_TICKET = "Ticket"
    
    // Event tracking
    //
    
    static let EVENT_CATEGORY_TV = "TV Programs"
    static let EVENT_CATEGORY_VIDEO_DETAIL = "Video Detail"
    static let EVENT_CATEGORY_REWARD_POINT = "Reward Point"
    static let EVENT_CATEGORY_REWARD_POINTS = "Reward Points"
    static let EVENT_CATEGORY_NEWS_DETAIL = "News Detail"
    static let EVENT_CATEGORY_LOGOUT = "Logout"
    
    static let EVENT_ACTION_BELI_TIKET = "Beli Tiket"
    static let EVENT_ACTION_VIEWED = "Viewed"
    static let EVENT_ACTION_CLICKED = "Clicked"
    static let EVENT_ACTION_SELECTED = "Selected"
    static let EVENT_ACTION_LIKED = "set_as_favorite"
    static let EVENT_ACTION_GET_REWARD_VOUCHER = "Get Reward Voucher"
    static let EVENT_ACTION_GET_REWARD_MERCHANDISE = "Get Reward Merchandise"
    
}

 */
