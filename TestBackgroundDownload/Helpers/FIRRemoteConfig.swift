//
//  FIRRemoteConfig.swift
//  Klink Ecommerce
//
//  Created by Alam Akbar Muhammad on 17/01/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

/*
import Foundation
import Firebase

class FIRRemoteConfig {
    
    static let instance = FIRRemoteConfig()
    
    var forceAlert: UIAlertController?
    
    func shouldForceUpdate() {
        let remoteConfig = RemoteConfig.remoteConfig()
        remoteConfig.setDefaults([
            "minimum_info_ios": 0 as NSObject,
            "minimum_force_ios": 0 as NSObject,
            "info_message": "" as NSObject,
            "force_message": "" as NSObject,
            "store_url_ios": "" as NSObject
            ])
        
        var developerModeEnabled = true
        var expirationDuration: TimeInterval = 0 // in seconds (do not use for release and production)
        #if RELEASE
            if ApiConfig.isProduction {
                developerModeEnabled = false
                expirationDuration = 21600 // 6 hours (firebase recommends 12h)
            }
        #endif
        let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: developerModeEnabled)
        remoteConfig.configSettings = remoteConfigSettings!
        remoteConfig.fetch(withExpirationDuration: expirationDuration, completionHandler: {(status, error) in
            switch status {
            case .success :
                print("KlinkRemoteConfig: success")
                remoteConfig.activateFetched()
                break
            case .failure :
                print("KlinkRemoteConfig: failure")
                print(String(describing: error?.localizedDescription))
                break
            default :
                break
            }
            
            let buildVersionNormal = remoteConfig.configValue(forKey: "minimum_info_ios").stringValue ?? "0"
            let buildVersionForce = remoteConfig.configValue(forKey: "minimum_force_ios").stringValue ?? "0"
            let buildVersionNormalMessage = remoteConfig.configValue(forKey: "info_message").stringValue ?? ""
            let buildVersionForceMessage = remoteConfig.configValue(forKey: "force_message").stringValue ?? ""
            let appstoreUrl = remoteConfig.configValue(forKey: "store_url_ios").stringValue
            print("minimum_info_ios \(buildVersionNormal)")
            print("minimum_force_ios \(buildVersionForce)")
            print("store_url_ios \(appstoreUrl ?? "")")
            
            PreferenceManager.instance.appstoreUrl = appstoreUrl
            
            self.checkBuild(buildVersionForce: buildVersionForce, buildVersionNormal: buildVersionNormal, buildVersionForceMessage: buildVersionForceMessage, buildVersionNormalMessage: buildVersionNormalMessage)
        })
    }
    
    func checkBuild(buildVersionForce: String, buildVersionNormal: String, buildVersionForceMessage: String, buildVersionNormalMessage: String) {
        if let infoDict = Bundle.main.infoDictionary {
            let buildVersion = Int(infoDict["CFBundleVersion"] as! String)!
            
            forceAlert?.dismiss(animated: false, completion: nil)
            
            if let buildVersionForceNumber = Int(buildVersionForce), buildVersion < buildVersionForceNumber {
                // Alert
                forceAlert = UIAlertController(title: "UPDATE INFO", message: "\(buildVersionForceMessage)", preferredStyle: UIAlertControllerStyle.alert)
                let yesButton = UIAlertAction(title: "Update", style: UIAlertActionStyle.default, handler: { alertAction in
                    UIApplication.shared.openURL(NSURL(string: PreferenceManager.instance.appstoreUrl ?? "")! as URL)
                })
                forceAlert?.addAction(yesButton)
                let vc = AppDelegate.getRootViewController()
                vc?.present(forceAlert!, animated: true, completion: nil)
                return
            }
            
            if let buildVersionNormalNumber = Int(buildVersionNormal), buildVersion < buildVersionNormalNumber {
                // Alert
                forceAlert = UIAlertController(title: "UPDATE INFO", message: "\(buildVersionNormalMessage)", preferredStyle: UIAlertControllerStyle.alert)
                let yesButton = UIAlertAction(title: "Update", style: UIAlertActionStyle.default, handler: { alertAction in
                    // Change the url
                    UIApplication.shared.openURL(NSURL(string: PreferenceManager.instance.appstoreUrl ?? "")! as URL)
                })
                let laterButton = UIAlertAction(title: "Later", style: UIAlertActionStyle.default, handler: { alertAction in
                    self.forceAlert?.dismiss(animated: true, completion: nil)
                })
                forceAlert?.addAction(yesButton)
                forceAlert?.addAction(laterButton)
                
                let vc = AppDelegate.getRootViewController()
                vc?.present(forceAlert!, animated: true, completion: nil)
                return
            }
        }
    }
    
}
*/
