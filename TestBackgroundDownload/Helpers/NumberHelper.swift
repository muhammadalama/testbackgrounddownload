//
//  NumberHelper.swift
//  Created by Rifat Firdaus on 2/14/17.
//

import UIKit

class NumberHelper: NSObject {

    static let instance = NumberHelper()

    func format(currency: String? = nil, value: Double, fraction: Int = 0) -> String {
        var currencyPrefix = ""
        if let currency = currency {
            currencyPrefix = currency
        }
        var result = ""
        let numberFormatter = NumberFormatter()
        numberFormatter.currencyCode = " "
        if #available(iOS 9.0, *) {
            numberFormatter.numberStyle = .decimal
        } else {
            // Fallback on earlier versions
            numberFormatter.numberStyle = .decimal
        }
        numberFormatter.minimumFractionDigits = fraction
        numberFormatter.maximumFractionDigits = fraction
        numberFormatter.decimalSeparator = ","
        numberFormatter.groupingSeparator = "."
//        numberFormatter.currencyGroupingSeparator = "."
//        numberFormatter.currencyDecimalSeparator = ","
        result = numberFormatter.string(from: NSNumber(value: value))!
        return "\(currencyPrefix) \(result)"
    }
    
    func formatDecimal(currency: String? = nil, value: Int64, fraction: Int = 0) -> String {
        var currencyPrefix = ""
        if let currency = currency {
            currencyPrefix = currency
        }
        var result = ""
        let numberFormatter = NumberFormatter()
        numberFormatter.currencyCode = " "
        numberFormatter.numberStyle = .decimal
        numberFormatter.minimumFractionDigits = fraction
        numberFormatter.maximumFractionDigits = fraction
        result = numberFormatter.string(from: NSNumber(value: value))!
        return "\(currencyPrefix) \(result)"
    }
    
    func unformat(value: String) -> String {
        return value.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
    }
    
}
