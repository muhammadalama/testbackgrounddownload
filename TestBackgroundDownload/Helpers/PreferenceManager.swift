//
//  PreferenceManager.swift
//
//
//  Created by Rifat Firdaus on 9/8/16.
//  Copyright © 2016 Suitmedia. All rights reserved.
//

import Foundation
import SuitmobileCore

class PreferenceManager: NSObject {
    
    private static let Token = "token"
    private static let Country = "country"
    private static let Userid = "id"
    private static let Vacancyid = "id"
    private static let IDX = "IDX"
    private static let firstLaunch = "firstLaunch"
    private static let CoachMarkShown = "CochMarkShown"
    private static let Tipsid = "tipsid"
    private static let Bannersid = "bannersid"
    private static let PlaylistId = "PlaylistId"
    private static let SelectedIndex = "SelectedIndex"
    private static let BuildVersion = "BuildVersion"
    private static let Currency = "currency"
    private static let Language = "lang"
    private static let FCMToken = "fcm_token"
    private static let EmailLogin = "email_login"
    private static let Live = "Live"
    private static let Upcoming = "Upcoming"
    private static let Latest = "Latest"
    private static let News = "News"
    private static let Video = "Video"
    private static let FisrstLaunch = "FisrstLaunch"
    private static let FromVoucher = "FromVoucher"
    private static let ConfirmPayment = "ConfirmPayment"
    private static let FromVoucherAllReward = "FromVoucherAllReward"
    private static let FromUpcoming = "FromUpcoming"
    private static let Endpoint = "Endpoint"
    private static let NotifCounter = "notifCounter"
    
    private static let DeeplinkType = "deeplink_type"
    private static let DeeplinkId = "deeplink_id"
    
    static let instance = PreferenceManager()
    
    private let userDefaults: UserDefaults
    
    override init() {
        userDefaults = UserDefaults.standard
    }
    
    init(userDefaults: UserDefaults) {
        self.userDefaults = userDefaults
    }
    
    var endpoint: String? {
        get {
            return userDefaults.string(forKey: PreferenceManager.Endpoint)
        }
        set(newEndpoint) {
            if let endpoint = newEndpoint {
                userDefaults.set(endpoint, forKey: PreferenceManager.Endpoint)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Endpoint)
            }
        }
    }
    
    var token: String? {
        get {
            return userDefaults.string(forKey: PreferenceManager.Token)
        }
        set(newToken) {
            if let token = newToken {
                userDefaults.set(token, forKey: PreferenceManager.Token)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Token)
            }
        }
    }
    
    var userid: Int64? {
        get {
            return userDefaults.object(forKey: PreferenceManager.Userid) as? Int64
        }
        set(newUserid) {
            if let userid = newUserid {
                userDefaults.set(userid, forKey: PreferenceManager.Userid)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Userid)
            }
        }
    }
    
    var vacancyid: Int64? {
        get {
            return userDefaults.object(forKey: PreferenceManager.Vacancyid) as? Int64
        }
        set(newVacancyid) {
            if let vacancyid = newVacancyid {
                userDefaults.set(vacancyid, forKey: PreferenceManager.Vacancyid)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Vacancyid)
            }
        }
    }
    
    var tipsid: Int64? {
        get {
            return userDefaults.object(forKey: PreferenceManager.Tipsid) as? Int64
        }
        set(newTipsid) {
            if let tipsid = newTipsid {
                userDefaults.set(tipsid, forKey: PreferenceManager.Tipsid)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Tipsid)
            }
        }
    }
    
    var bannersid: Int64? {
        get {
            return userDefaults.object(forKey: PreferenceManager.Bannersid) as? Int64
        }
        set(newBannersid) {
            if let bannersid = newBannersid {
                userDefaults.set(bannersid, forKey: PreferenceManager.Bannersid)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Bannersid)
            }
        }
    }
    
    var playlistId: Int64? {
        get {
            return userDefaults.object(forKey: PreferenceManager.PlaylistId) as? Int64
        }
        set(newPlaylistId) {
            if let playlistId = newPlaylistId {
                userDefaults.set(playlistId, forKey: PreferenceManager.PlaylistId)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.PlaylistId)
            }
        }
    }
    
    var selectedIndex: Int? {
        get {
            return userDefaults.object(forKey: PreferenceManager.SelectedIndex) as? Int
        }
        set(newSelectedIndex) {
            if let selectedIndex = newSelectedIndex {
                userDefaults.set(selectedIndex, forKey: PreferenceManager.SelectedIndex)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.SelectedIndex)
            }
        }
    }
    
    var buildVersion: Int? {
        get {
            return userDefaults.object(forKey: PreferenceManager.BuildVersion) as? Int
        }
        set(newBuildVersion) {
            if let buildVersion = newBuildVersion {
                userDefaults.set(buildVersion, forKey: PreferenceManager.BuildVersion)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.BuildVersion)
            }
        }
    }
    
    var country: String? {
        get {
            return userDefaults.string(forKey: PreferenceManager.Country)
        }
        set(newCountry) {
            if let country = newCountry {
                userDefaults.set(country, forKey: PreferenceManager.Country)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Country)
            }
        }
    }
    
    var IDX: String? {
        get {
            return userDefaults.string(forKey: PreferenceManager.IDX)
        }
        set(newIDX) {
            if let IDX = newIDX {
                userDefaults.set(IDX, forKey: PreferenceManager.IDX)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.IDX)
            }
        }
    }
    
    var firstLaunch: Bool? {
        get {
            if let firstLaunch = userDefaults.object(forKey: PreferenceManager.firstLaunch) as? Bool {
                return firstLaunch
            }
            return false
        }
        set(newFirstLaunch) {
            if let firstLaunch = newFirstLaunch {
                userDefaults.set(firstLaunch, forKey: PreferenceManager.firstLaunch)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.firstLaunch)
            }
        }
    }
    
    var coachMarkShown: Bool? {
        get {
            if let coachMarkShown = userDefaults.object(forKey: PreferenceManager.CoachMarkShown) as? Bool {
                return coachMarkShown
            }
            return false
        }
        set(newCoachMarkShown) {
            if let coachMarkShown = newCoachMarkShown {
                userDefaults.set(coachMarkShown, forKey: PreferenceManager.CoachMarkShown)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.CoachMarkShown)
            }
        }
    }
    
    var language: String? {
        get {
            return userDefaults.string(forKey: PreferenceManager.Language)
        }
        set(newLanguage) {
            if let language = newLanguage {
                userDefaults.set(language, forKey: PreferenceManager.Language)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Language)
            }
        }
    }
    
    var currency: String? {
        get {
            return userDefaults.string(forKey: PreferenceManager.Currency)
        }
        set(newCurrency) {
            if let currency = newCurrency {
                userDefaults.set(currency, forKey: PreferenceManager.Currency)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Currency)
            }
        }
    }
    
    var FCMToken: String? {
        get {
            return userDefaults.string(forKey: PreferenceManager.FCMToken)
        }
        set(FCMToken) {
            if let FCMToken = FCMToken {
                userDefaults.set(FCMToken, forKey: PreferenceManager.FCMToken)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.FCMToken)
            }
        }
    }
    
    var isLoggedInUsingEmail: Bool? {
        get {
            if let isLoggedInUsingEmail = userDefaults.object(forKey: PreferenceManager.EmailLogin) as? Bool {
                return isLoggedInUsingEmail
            }
            return false
        }
        set(newIsLoggedInUsingEmail) {
            if let isLoggedInUsingEmail = newIsLoggedInUsingEmail {
                userDefaults.set(isLoggedInUsingEmail, forKey: PreferenceManager.EmailLogin)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.EmailLogin)
            }
        }
    }
    
    var isSubscribeLive: Bool? {
        get {
            if let isSubscribeLive = userDefaults.object(forKey: PreferenceManager.Live) as? Bool {
                return isSubscribeLive
            }
            return false
        }
        set(newIsSubscribeLive) {
            if let isSubscribeLive = newIsSubscribeLive {
                userDefaults.set(isSubscribeLive, forKey: PreferenceManager.Live)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Live)
            }
        }
    }
    
    var isSubscribeLatest: Bool? {
        get {
            if let isSubscribeLatest = userDefaults.object(forKey: PreferenceManager.Latest) as? Bool {
                return isSubscribeLatest
            }
            return false
        }
        set(newIsSubscribeLatest) {
            if let isSubscribeLatest = newIsSubscribeLatest {
                userDefaults.set(isSubscribeLatest, forKey: PreferenceManager.Latest)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Latest)
            }
        }
    }
    
    var isSubscribeUpcoming: Bool? {
        get {
            if let isSubscribeUpcoming = userDefaults.object(forKey: PreferenceManager.Upcoming) as? Bool {
                return isSubscribeUpcoming
            }
            return false
        }
        set(newIsSubscribeUpcoming) {
            if let isSubscribeUpcoming = newIsSubscribeUpcoming {
                userDefaults.set(isSubscribeUpcoming, forKey: PreferenceManager.Upcoming)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Upcoming)
            }
        }
    }
    
    var isSubscribeNews: Bool? {
        get {
            if let isSubscribeNews = userDefaults.object(forKey: PreferenceManager.News) as? Bool {
                return isSubscribeNews
            }
            return false
        }
        set(newIsSubscribeNews) {
            if let isSubscribeNews = newIsSubscribeNews {
                userDefaults.set(isSubscribeNews, forKey: PreferenceManager.News)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.News)
            }
        }
    }
    
    var isSubscribeVideo: Bool? {
        get {
            if let isSubscribeVideo = userDefaults.object(forKey: PreferenceManager.Video) as? Bool {
                return isSubscribeVideo
            }
            return false
        }
        set(newIsSubscribeVideo) {
            if let isSubscribeVideo = newIsSubscribeVideo {
                userDefaults.set(isSubscribeVideo, forKey: PreferenceManager.Video)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.Video)
            }
        }
    }
    
    var launced: Bool? {
        get {
            if let launced = userDefaults.object(forKey: PreferenceManager.FisrstLaunch) as? Bool {
                return launced
            }
            return false
        }
        set(newlaunced) {
            if let launced = newlaunced {
                userDefaults.set(launced, forKey: PreferenceManager.FisrstLaunch)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.FisrstLaunch)
            }
        }
    }
    
    var isFromVoucher: Bool? {
        get {
            if let launced = userDefaults.object(forKey: PreferenceManager.FromVoucher) as? Bool {
                return launced
            }
            return false
        }
        set(newlaunced) {
            if let launced = newlaunced {
                userDefaults.set(launced, forKey: PreferenceManager.FromVoucher)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.FromVoucher)
            }
        }
    }
    
    var isConfirmPayment: Bool? {
        get {
            if let launced = userDefaults.object(forKey: PreferenceManager.ConfirmPayment) as? Bool {
                return launced
            }
            return false
        }
        set(newlaunced) {
            if let launced = newlaunced {
                userDefaults.set(launced, forKey: PreferenceManager.ConfirmPayment)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.ConfirmPayment)
            }
        }
    }
    
    var FromVoucherAllReward: Bool? {
        get {
            if let launced = userDefaults.object(forKey: PreferenceManager.FromVoucherAllReward) as? Bool {
                return launced
            }
            return false
        }
        set(newlaunced) {
            if let launced = newlaunced {
                userDefaults.set(launced, forKey: PreferenceManager.FromVoucherAllReward)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.FromVoucherAllReward)
            }
        }
    }
    
    var isFromUpcoming: Bool? {
        get {
            if let launced = userDefaults.object(forKey: PreferenceManager.FromUpcoming) as? Bool {
                return launced
            }
            return false
        }
        set(newlaunced) {
            if let launced = newlaunced {
                userDefaults.set(launced, forKey: PreferenceManager.FromUpcoming)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.FromUpcoming)
            }
        }
    }
    
    var deeplinkType: String? {
        get {
            return userDefaults.string(forKey: PreferenceManager.DeeplinkType)
        }
        set(newDeeplinkType) {
            if let deeplinkType = newDeeplinkType {
                userDefaults.set(deeplinkType, forKey: PreferenceManager.DeeplinkType)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.DeeplinkType)
            }
        }
    }
    
    var deeplinkId: String? {
        get {
            return userDefaults.string(forKey: PreferenceManager.DeeplinkId)
        }
        set(newDeeplinkId) {
            if let deeplinkId = newDeeplinkId {
                userDefaults.set(deeplinkId, forKey: PreferenceManager.DeeplinkId)
            } else {
                userDefaults.removeObject(forKey: PreferenceManager.DeeplinkId)
            }
        }
    }
    
    var notifCounter: Int64 {
        get {
            if let notif = userDefaults.object(forKey: PreferenceManager.NotifCounter) as? Int64 {
                return notif
            }
            return 0
        }
        set(newNotifCounter) {
            userDefaults.set(newNotifCounter, forKey: PreferenceManager.NotifCounter)
        }
    }

}
