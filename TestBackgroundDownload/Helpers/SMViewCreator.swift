//
//  SMViewCreator.swift
//  CommHub
//
//  Created by Rifat Firdaus on 4/3/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import UIKit

class SMViewCreator: NSObject {
    
    static func createAlert(title: String? = nil, message: String? = nil, agreeText: String? = "OK", agreeHandler: ((UIAlertAction) -> Swift.Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: agreeText, style: .cancel, handler: agreeHandler))
        return alert
    }
    
    static func createAlertDialog(title: String?, message: String?, agreeText: String? = "OK", cancelText: String? = "Cancel", agreeHandler: ((UIAlertAction) -> Swift.Void)? = nil, cancelHandler: ((UIAlertAction) -> Swift.Void)? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: agreeText, style: .default, handler: agreeHandler))
        alert.addAction(UIAlertAction(title: cancelText, style: .cancel, handler: cancelHandler))
        return alert
    }
    
    static func showAlert(sender controller: UIViewController, title: String?, message: String?, titlePositive: String?, firstHandler positiveHandler: ((UIAlertAction) -> (Void))? = nil) {
        let alert = createAlert(title: title, message: message, agreeText: titlePositive, agreeHandler: positiveHandler)
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func showAlertDialog(sender controller: UIViewController, title: String?, message: String?, titlePositive: String?, titleNegative: String?, firstHandler positiveHandler: ((UIAlertAction) -> (Void))? = nil, secondHandler negativeHandler: ((UIAlertAction) -> (Void))? = nil) {
        let alert = createAlertDialog(title: title, message: message, agreeText: titlePositive, cancelText: titleNegative, agreeHandler: positiveHandler, cancelHandler: negativeHandler)
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func createNavigationController(rootController controller: UIViewController, transparent: Bool = true) -> UINavigationController {
        let nav = UINavigationController(rootViewController: controller)
        nav.navigationBar.barStyle = .black
        nav.navigationBar.tintColor = UIColor.white
        //        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.yellow]
        //        nav.navigationBar.titleTextAttributes = textAttributes
        if transparent {
            nav.navigationBar.backgroundColor = nil
            nav.navigationBar.setBackgroundImage(UIImage(), for: .default)
            nav.navigationBar.shadowImage = UIImage()
            nav.navigationBar.isTranslucent = true
        } else {
            nav.navigationBar.isTranslucent = false
            nav.navigationBar.barTintColor = UIColor(hex: 0x8cc1f1)
        }
        return nav
    }
    
    /*
    // import ActionSheetPicker_3_0
    static func createActionSheet(title: String? = nil, message: String? = nil,actions: UIAlertAction...) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        actions.forEach { alertAction in
            alert.addAction(alertAction)
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        //        self.present(alert, animated: true, completion: nil)
        return alert
    }
    
    static func datePicker(origin: UIView?, onDone: ((Date) -> (Void))? = nil, onCancel: (() -> (Void))? = nil) -> ActionSheetDatePicker? {
        let datePicker = ActionSheetDatePicker(title: "", datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
            picker, value, index in
            
            //print("value = \(value)")
            //print("index = \(index)")
            //print("picker = \(picker)")
            //let dateStr = (value as! Date).string(custom: "dd-MM-yyyy")
            onDone?((value as! Date))
            return
        }, cancel: { ActionStringCancelBlock in
            onCancel?()
            return
        }, origin: origin)
        //let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
        //datePicker?.minimumDate = Date(timeInterval: -secondsInWeek, since: Date())
        //datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
        return datePicker
    }
    */
    
}
