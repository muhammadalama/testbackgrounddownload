//
//  ShareManager.swift
//  Bali United
//
//  Created by Muhammad Alam Akbar on 3/23/17.
//  Copyright © 2017 Suitmedia. All rights reserved.
//

import UIKit

class ShareManager {

    public static let customAllowedSet =  NSCharacterSet(charactersIn:"&=\"#%/<>?@\\^`{|}").inverted
    
    public static func generateDeeplink(_ link: String) -> String {
        let dynamicLink = "https://rkpw9.app.goo.gl/?link=\(link)&apn=com.baliutd.android&afl=\(link)&ifl=\(link)"
//        print("generateDeeplink \(dynamicLink)")
        return dynamicLink
    }
    
    public static func generateDeeplink(withNewsSlag slag: String, id: Int64) -> String {
        let link = "http://www.baliutd.com/news/\(slag)/\(id)"
        let dynamicLink = "https://rkpw9.app.goo.gl/?link=\(link)&apn=com.baliutd.android&afl=\(link)&ifl=\(link)"
//        print("generateDeeplinkwithNewsSlag \(dynamicLink)")
        return dynamicLink
    }
    
    public static func performShare(viewController: UIViewController, wording: String) {
        let items = [
            wording
        ]
        let controller = UIActivityViewController(activityItems: items, applicationActivities: nil)
        controller.excludedActivityTypes = [UIActivityType.assignToContact, UIActivityType.copyToPasteboard, UIActivityType.postToWeibo, UIActivityType.print]
        
        if controller.responds(to: #selector(getter: UIViewController.popoverPresentationController)) {
            // iOS8+
            viewController.present(controller, animated: true, completion: nil)
            controller.popoverPresentationController?.sourceView = viewController.view
        } else {
            viewController.present(controller, animated: true, completion: nil)
        }
    }
    
}
