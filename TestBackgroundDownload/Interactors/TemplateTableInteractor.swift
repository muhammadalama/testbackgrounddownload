//
//  TemplateTableInteractor.swift
//  TestBackgroundDownload
//
//  Created by Rifat Firdaus on 2/21/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SuitmobileCore
import Alamofire

class TemplateTableInteractor: BaseInteractor {
    
    var refreshSubject = PublishSubject<Void>()
    
    func testDownload1() -> Observable<DownloadRequest> {
        return service.downloadTest1()
    }
    
    func testDownload2(onSuccess: @escaping () -> (Void), onError: @escaping (Error) -> (Void)) {
        return service.downloadTest2(onSuccess: onSuccess, onError: onError)
    }
    
}
