//
//  News.swift
//  TestBackgroundDownload
//
//  Created by Rifat Firdaus on 2/21/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyJSON
import SuitmobileCore

class News: Object {
    
    // Akan di update
    
    @objc dynamic public var identifier: Int64 = 0
    @objc dynamic public var type: String?
    @objc dynamic public var title: String?
    @objc dynamic public var slug: String?
    @objc dynamic public var image: String?
    @objc dynamic public var videoUrl: String?
    @objc dynamic public var videoStatus: String?
    @objc dynamic public var videoStatusDescription: String?
    @objc dynamic public var totalLike: String?
    @objc dynamic public var content: String?
    @objc dynamic public var publishedAt: Date?
    @objc dynamic public var createdAt: Date?
    @objc dynamic public var updatedAt: Date?
    
    override public static func primaryKey() -> String? {
        return "identifier"
    }
    
    public static func with(realm: Realm, json: JSON) -> News? {
        let identifier = json["id"].int64Value
        if identifier == 0 {
            return nil
        }
        var news = realm.object(ofType: News.self, forPrimaryKey: identifier)
        if news == nil {
            news = News()
            news?.identifier = identifier
        } else {
            news = News(value: news!)
        }
        if json["slug"].exists() {
            news?.slug = json["slug"].string
        }
        if json["type"].exists() {
            news?.type = json["type"].string
        }
        if json["title"].exists() {
            news?.title = json["title"].string
        }
        if json["slug"].exists() {
            news?.slug = json["slug"].string
        }
        if json["image"].exists() {
            news?.image = json["image"].string
        }
        if json["video_url"].exists() {
            news?.videoUrl = json["video_url"].string
        }
        if json["video_status"].exists() {
            news?.videoStatus = json["video_status"].string
        }
        if json["video_status_description"].exists() {
            news?.videoStatusDescription = json["video_status_description"].string
        }
        if json["content"].exists() {
            news?.content = json["content"].string
        }
        if json["total_like"].exists() {
            news?.totalLike = json["total_like"].string
        }
        if json["published_at"].exists() {
            news?.publishedAt = DateHelper.iso8601(dateString: json["published_at"].stringValue)
        }
        if json["created_at"].exists() {
            news?.createdAt = DateHelper.iso8601(dateString: json["created_at"].stringValue)
        }
        if json["updated_at"].exists() {
            news?.updatedAt = DateHelper.iso8601(dateString: json["updated_at"].stringValue)
        }
        
        return news
    }
    
    public static func with(json: JSON) -> News? {
        return with(realm: try! Realm(), json: json)
    }
}
