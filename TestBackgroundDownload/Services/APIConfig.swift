//
//  KlinkApi.swift
//  Klink Ecommerce
//
//  Created by Alam Akbar M on 10/5/17.
//  Copyright © 2017 Suitmedia. All rights reserved.
//

import Foundation

class APIConfig {
    
    static let tokenKey = "123"
    
    static var isProduction = true {
        didSet {
            print("ApiConfig: \(isProduction ? "Production" : "Development")")
        }
    }
    
    static var API_HOME: String {
        if !isProduction {
            // staging
            return "http://"
        } else {
            // production
            return "http://"
        }
    }
    
    /* Payment Gateway */
    static var API_PG_URL: String {
        if !isProduction {
            // staging
            return "https://"
        } else {
            // production
            return "https://"
        }
    }
    
    static var API_PG_KEY: String {
        if !isProduction {
            // staging
            return "123"
        } else {
            // production
            return "123"
        }
    }
    
}
