//
//  SuitCore+Extensions.swift
//
//
//  Created by Rifat Firdaus on 11/18/16.
//  Copyright © 2016 Suitmedia. All rights reserved.
//

import UIKit
import SuitmobileCore
import SwiftyJSON
import Alamofire

import RxSwift
import RxCocoa

struct APIResponse {
    var message: String?
    var code: Int?
    var result: JSON?
}

class SMRequestDownload {
    
    static let shared = SMRequestDownload()
    
    var window: UIWindow
    private var manager: SessionManager?
    private var request: DownloadRequest? {
        didSet {
            guard let request = request else { return }
            
            let infoController = window.rootViewController as! DownloadInfoViewController
            request.downloadProgress(closure: { [weak infoController] progress in
                let value = Float(progress.fractionCompleted * 100)
                print(value)
                infoController?.textLabel.text = "Progress: \(value)"
            })
        }
    }
    
    init() {
        window = UIWindow(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 80, width: UIScreen.main.bounds.width, height: 80))
        
        let infoController = DownloadInfoViewController()
        infoController.view.backgroundColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        
        window.rootViewController = infoController
        //window.windowLevel = UIWindowLevelStatusBar
    }
    
    func register(manager: SessionManager, request: DownloadRequest) {
        self.manager = manager
        self.request = request
    }
    
    func show() {
        window.makeKeyAndVisible()
    }
    
}

extension SuitmobileCoreService {
    
    func downloadTest1() -> Observable<DownloadRequest> {
        // 95mb
        //let url = URL(string: "http://mirrors.standaloneinstaller.com/video-sample/jellyfish-25-mbps-hd-hevc.mkv")!
        
        // 2mb
        let url = URL(string: "http://mirrors.standaloneinstaller.com/video-sample/star_trails.avi")!
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(url.lastPathComponent)
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (fileURL, [.removePreviousFile])
        }
        
        let urlRequest = try! URLRequest(url: url, method: .get)
        
        let configuration = URLSessionConfiguration.background(withIdentifier: "com.axu.testbackgrounddownload.background")
        let backgroundManager = SuitmobileSessionManager(configuration: configuration)
        return backgroundManager.download(urlRequest: urlRequest, to: destination)
            .do(onNext: { request in
                request.downloadProgress(closure: { progress in
                    let value = Int(progress.fractionCompleted * 100)
                    print("progress: \(value)%")
                })
                
                request.response(completionHandler: { response in
                    print("completed \(response.destinationURL!)")
                })
                
                // log print wil be overrided after request registered in SMRequestDownload manager.
                SMRequestDownload.shared.register(manager: backgroundManager, request: request)
            })
            .do(onError: { error in
                print("\(error.localizedDescription)")
            })
            .do(onCompleted: {
                print("completed")
                backgroundManager.session.finishTasksAndInvalidate()
            })
    }
    
    func downloadTest2(onSuccess: @escaping () -> (Void), onError: @escaping (Error) -> (Void)) {
        // 95mb
        //let url = URL(string: "http://mirrors.standaloneinstaller.com/video-sample/jellyfish-25-mbps-hd-hevc.mkv")!
        
        // 2mb
        let url = URL(string: "http://mirrors.standaloneinstaller.com/video-sample/star_trails.avi")!
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileURL = documentsURL.appendingPathComponent(url.lastPathComponent)
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (fileURL, [.removePreviousFile])
        }
        
        let configuration = URLSessionConfiguration.background(withIdentifier: "com.axu.testbackgrounddownload.background")
        let backgroundManager = SessionManager(configuration: configuration)
        //backgroundManager.startRequestsImmediately = true
        
        let request = backgroundManager.download(url.absoluteString, to: destination)
        request.response(completionHandler: { response in
            if response.error == nil {
                print("completed: \(response.destinationURL!)")
                onSuccess()
            } else {
                onError(response.error!)
            }
            backgroundManager.session.finishTasksAndInvalidate()
        })
        
        // log print wil be overrided after request registered in SMRequestDownload manager.
        SMRequestDownload.shared.register(manager: backgroundManager, request: request)
    }
    
}
