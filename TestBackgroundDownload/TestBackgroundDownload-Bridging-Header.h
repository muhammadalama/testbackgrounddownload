//
//  TestBackgroundDownload-Bridging-Header.h
//  TestBackgroundDownload
//
//  Created by Alam Akbar Muhammad on 06/08/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

#import "Externals/MXPagerView/MXPagerView.h"
#import "Externals/MXPagerView/MXPagerViewController.h"

#import "Externals/MXParallaxHeader/MXParallaxHeader.h"
#import "Externals/MXParallaxHeader/MXScrollView.h"
#import "Externals/MXParallaxHeader/MXScrollViewController.h"

#import "Externals/HMSegmentedControl/HMSegmentedControl.h"

#import "Externals/MXSegmentedPager/MXSegmentedPager.h"
#import "Externals/MXSegmentedPager/MXSegmentedPagerController.h"
