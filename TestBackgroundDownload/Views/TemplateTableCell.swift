//
//  TemplateTableCell.swift
//  TestBackgroundDownload
//
//  Created by Rifat Firdaus on 2/21/18.
//  Copyright © 2018 Suitmedia. All rights reserved.
//

import UIKit

class TemplateTableCell: UITableViewCell {

    @IBOutlet var labelTitle: UILabel!
    
    var indexPath: IndexPath? {
        didSet {
            if let indexPath = indexPath {
                labelTitle.text = "Index \(indexPath.section) - \(indexPath.row)"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
